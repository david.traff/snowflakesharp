﻿using System;
using System.Threading.Tasks;

namespace Snowflaker.Testprogram
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var generator = new RemoteSnowflakeGenerator("https://snowflake.davidtraff.com");

            while (true)
            {
                var snowflake = await generator.GenerateAsync(1000);

                foreach (var flake in snowflake)
                    Console.WriteLine(flake);

                Console.ReadLine();
            }
        }
    }
}
