using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Snowflaker.Test
{
    public class RemoteGeneratorTests
    {
        private readonly string _Endpoint = "https://snowflake.davidtraff.com";

        [Fact]
        public void Generates_Multiple_Fails_No_Initialize()
        {
            var generator = new RemoteSnowflakeGenerator(_Endpoint);

            Assert.ThrowsAsync<SnowflakeGenerationException>(async () => await generator.GenerateAsync(1000));
        }

        [Fact]
        public async void Generates_Single()
        {
            var generator = new RemoteSnowflakeGenerator(_Endpoint);

            var data = await generator.GenerateAsync();

            Assert.True(data.Value > 0);
            Assert.Equal(1, data.Machine);
        }

        [Fact]
        public async void Generates_Multiple_Under_Max()
        {
            var generator = new RemoteSnowflakeGenerator(_Endpoint);

            var data = await generator.GenerateAsync(1000);

            Assert.Equal(1000, data.Count());
            Snowflake previous = new Snowflake(0, 0);
            foreach(var flake in data)
            {
                Assert.Equal(1, flake.Machine);
                Assert.True(flake.Value > 0);

                // We test this to make sure we get the flakes in one go.
                if (previous.Value > 0)
                    Assert.True(flake.SequenceNumber > previous.SequenceNumber);

                previous = flake;
            }
        }

        [Fact]
        public async void Generates_Multiple_Over_Max()
        {
            var generator = new RemoteSnowflakeGenerator(_Endpoint);

            var data = await generator.GenerateAsync(10000);

            Assert.Equal(10000, data.Count());
            foreach (var flake in data)
            {
                Assert.Equal(1, flake.Machine);
                Assert.True(flake.Value > 0);
            }
        }
    }
}
