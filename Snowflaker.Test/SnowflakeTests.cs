﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Snowflaker.Test
{
    public class SnowflakeTests
    {
        [Fact]
        public void Value_Returns_Value()
        {
            var snowflake = new Snowflake(1, 0);

            Assert.Equal(1UL, snowflake.Value);
        }

        [Fact]
        public void Machine_Only_Returns_Machine()
        {
            var snowflake = new Snowflake(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00111111, 0);

            Assert.Equal(63UL, snowflake.Value);
            Assert.Equal(63UL, snowflake.Machine);
            Assert.Equal(0UL, snowflake.SequenceNumber);
            Assert.Equal(0UL, snowflake.Timestamp);
        }

        [Fact]
        public void Sequence_Only_Returns_Sequence()
        {
            var snowflake = new Snowflake(0b00000000_00000000_00000000_00000000_00000000_00111111_11111111_11000000, 0);

            Assert.Equal(4194240UL, snowflake.Value);
            Assert.Equal(0, snowflake.Machine);
            Assert.Equal(65535UL, snowflake.SequenceNumber);
            Assert.Equal(0UL, snowflake.Timestamp);
        }

        [Fact]
        public void Time_Only_Returns_Time()
        {
            var snowflake = new Snowflake(0b11111111_11111111_11111111_11111111_11111111_11000000_00000000_00000000, 0);

            Assert.Equal(18446744073705357312UL, snowflake.Value);
            Assert.Equal(0, snowflake.Machine);
            Assert.Equal(0, snowflake.SequenceNumber);
            Assert.Equal(4398046511103UL, snowflake.Timestamp);
        }

        [Fact]
        public void Snowflake_Maxvalue()
        {
            var snowflake = new Snowflake(0b11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111, 0);

            Assert.Equal(18446744073709551615UL, snowflake.Value);
            Assert.Equal(63UL, snowflake.Machine);
            Assert.Equal(65535UL, snowflake.SequenceNumber);
            Assert.Equal(4398046511103UL, snowflake.Timestamp);
        }
    }
}
