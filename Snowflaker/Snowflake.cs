﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snowflaker
{
    public struct Snowflake
    {
        private const int TIME_BITS = 42;
        private const int SEQUENCE_BITS = 16;
        private const int MACHINE_BITS = 64 - TIME_BITS - SEQUENCE_BITS;

        private const int SEQUENCE_SHIFT = MACHINE_BITS;
        private const int TIME_SHIFT = SEQUENCE_SHIFT + SEQUENCE_BITS;

        private const ulong SEQUENCE_MASK = (-1L ^ (-1L << SEQUENCE_BITS)) << SEQUENCE_SHIFT;
        private const ulong MACHINE_MASK = -1L ^ (-1L << MACHINE_BITS);

        private static readonly DateTime _UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public Snowflake(ulong value, ulong epoch)
        {
            _Value = value;
            _Epoch = epoch;
        }

        private readonly ulong _Value;
        private readonly ulong _Epoch;

        /// <summary>
        /// The raw value of this snowflake.
        /// </summary>
        public ulong Value => _Value;

        /// <summary>
        /// Timestamp since the servers epoch.
        /// </summary>
        public ulong Timestamp => _Value >> TIME_SHIFT;

        /// <summary>
        /// Timestamp since 1970-01-01 00:00 (beginning of Unix time)
        /// </summary>
        public ulong AbsoluteTimestamp => Timestamp + _Epoch;

        public DateTime AbsoluteDateTime => _UnixEpoch.AddMilliseconds(AbsoluteTimestamp);

        /// <summary>
        /// The servers machine-number.
        /// </summary>
        public byte Machine => (byte)(_Value & MACHINE_MASK);

        /// <summary>
        /// The snowflake sequence-number.
        /// </summary>
        public ushort SequenceNumber => (ushort)((_Value & SEQUENCE_MASK) >> SEQUENCE_SHIFT);

        public override string ToString()
            => $"{_Value} (Timestamp: {Timestamp} ({AbsoluteDateTime} UTC), Machine: {Machine}, SequenceNr: {SequenceNumber})";

        public override bool Equals(object obj)
            => obj is Snowflake snowflake && _Value == snowflake._Value;

        public override int GetHashCode()
            => _Value.GetHashCode();

        public static implicit operator ulong(Snowflake flake) => flake._Value;
        public static implicit operator long(Snowflake flake) => (long)flake._Value;
        public static implicit operator DateTime(Snowflake flake) => flake.AbsoluteDateTime;
    }
}
