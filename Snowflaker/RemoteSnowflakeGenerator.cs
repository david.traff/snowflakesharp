﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Snowflaker
{
    public class RemoteSnowflakeGenerator : ISnowflakeGenerator
    {
        public RemoteSnowflakeGenerator(string endpoint) : this(null, endpoint)
        {

        }

        public RemoteSnowflakeGenerator(IHttpClientFactory factory, string endpoint)
        {
            _Client = factory?.CreateClient(endpoint) ?? new HttpClient();

            // Make sure we strip away any path/query-type stuff from the endpoint.
            _Endpoint = new Uri(endpoint);
            _Endpoint = new Uri($"{_Endpoint.Scheme}{Uri.SchemeDelimiter}{_Endpoint.Authority}");

            _RawEndpoint = new Uri(_Endpoint, BitConverter.IsLittleEndian ? "/raw/le" : "/raw/be");
        }

        private readonly HttpClient _Client;
        private readonly Uri _Endpoint;

        private readonly Uri _RawEndpoint;

        private ulong _Epoch;

        public Uri Endpoint => _Endpoint;
        public ulong Epoch => _Epoch;

        private async Task InitializeAsync()
        {
            try
            {
                var response = await _Client.GetAsync(new Uri(_Endpoint, "/config"));

                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync();
                var config = JsonSerializer.Deserialize<Config>(json);

                _Epoch = config.Epoch;

                if (_Epoch == 0)
                    throw new SnowflakeGenerationException("Cannot use this endpoint since it's epoch is 0 in unix time.");
            }
            catch(Exception e)
            {
                throw new SnowflakeGenerationException("Failed to initialize this generator. See inner exception.", e);
            }
        }

        public async ValueTask<Snowflake> GenerateAsync()
        {
            if (_Epoch == 0)
                await InitializeAsync();

            try
            {
                var response = await _Client.GetAsync(_RawEndpoint);

                response.EnsureSuccessStatusCode();

                var data = await response.Content.ReadAsByteArrayAsync();

                if (data.Length != sizeof(ulong))
                    throw new SnowflakeGenerationException($"The recieved data was incomplete or corrupt. Expected length {sizeof(ulong)} bytes and got {data.Length} bytes");

                var value = BitConverter.ToUInt64(data, 0);

                return new Snowflake(value, _Epoch);
            }
            catch (Exception e)
            {
                throw new SnowflakeGenerationException("Could not generate snowflake. See inner exception.", e);
            }
        }

        public async ValueTask<IEnumerable<Snowflake>> GenerateAsync(int amount)
        {
            if (_Epoch == 0)
                await InitializeAsync();

            try
            {
                var result = new Snowflake[amount];
                var recievedTotal = 0;
                while (recievedTotal < amount)
                {
                    var response = await _Client.GetAsync(new Uri(_RawEndpoint, $"?count={amount - recievedTotal}"));

                    response.EnsureSuccessStatusCode();

                    var data = await response.Content.ReadAsByteArrayAsync();
                    var recieved = data.Length / sizeof(ulong);

                    for (int i = 0; i < recieved; i++)
                    {
                        var offset = i + recievedTotal;
                        var value = BitConverter.ToUInt64(data, i * sizeof(ulong));

                        result[offset] = new Snowflake(value, _Epoch);
                    }

                    recievedTotal += recieved;
                }

                return result;
            }
            catch (Exception e)
            {
                throw new SnowflakeGenerationException("Could not generate snowflake. See inner exception.", e);
            }
        }

        public class Config
        {
            [JsonPropertyName("epoch")]
            public ulong Epoch { get; set; }

            [JsonPropertyName("machine_id")]
            public byte MachineId { get; set; }
        }
    }
}
