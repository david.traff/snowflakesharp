﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snowflaker
{
    public class SnowflakeGenerationException : Exception
    {
        public SnowflakeGenerationException(string message) : base(message)
        {

        }

        public SnowflakeGenerationException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
