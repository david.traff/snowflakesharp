﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Snowflaker
{
    public interface ISnowflakeGenerator
    {
        /// <summary>
        /// Generates a new <see cref="Snowflake"/>
        /// </summary>
        /// <returns>A new <see cref="Snowflake"/></returns>
        /// <exception cref="SnowflakeGenerationException">Throws an exception when there is a generation error.</exception>
        ValueTask<Snowflake> GenerateAsync();

        /// <summary>
        /// Generates multiple unique <see cref="Snowflake"/>s.
        /// </summary>
        /// <param name="amount">The amount of <see cref="Snowflake"/>s to generate.</param>
        /// <returns>An enumerable of unique <see cref="Snowflake"/>s</returns>
        /// <exception cref="SnowflakeGenerationException">Throws an exception when there is a generation error.</exception>
        ValueTask<IEnumerable<Snowflake>> GenerateAsync(int amount);
    }
}
